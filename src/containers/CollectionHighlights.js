
import CollectionCard from "components/CollectionCard";
import ScrollableGridList from "components/ScrollableGridList";
import { useMockState } from "MockContext";
import { shuffleArray } from "utils/array";


function CollectionHighlights(props) {
  const { collections } = useMockState();
  const shuffledCollections = shuffleArray(collections)
  return (
    <ScrollableGridList cols={1.3} cellHeight={353} spacing={8}>
      {shuffledCollections.map((collection) => (
        <CollectionCard key={collection.id} data={collection} />
      ))}
    </ScrollableGridList>
  )
}

export default CollectionHighlights;
