import { GridListTile, GridListTileBar } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Link from "components/Link";
import ScrollableGridList from "components/ScrollableGridList";
import { useActiveTab } from "hooks/useActiveTab";
import { useMockState } from "MockContext";
import getRandomItems from "utils/array";

const useStyles = makeStyles(theme => ({
  gridWrapper: {
    overflowX: 'auto',
    overflowY: 'hidden'
  },
  gridList: {
    flexWrap: 'nowrap',
    /* Workarount to prevent ignoring last child margin in flex grid */
    display: 'inline-flex',
    overflowX: 'visible',
    overflowY: 'hidden',
    width: 'max-content',
    padding: window.innerWidth < 600 ? theme.spacing(2.3, 2) : theme.spacing(2.3, 0)
  },
  gridListTile: {
    maxWidth: 108,
    minWidth: 108,
    [theme.breakpoints.up('sm')]: {
      maxWidth: 130,
      minWidth: 130,
    },
  },
  title: {
    color: theme.palette.grey[50],
    fontSize: theme.typography.body1.fontSize,
    fontWeight: theme.typography.fontWeightMedium,
    paddingBottom: theme.spacing(1),
    whiteSpace: 'pre-wrap',
    lineHeight: theme.typography.body1.lineHeight
  },
  titleBar: {
    textAlign: 'center',
    alignItems: 'flex-end',
    height: '100%',
    background: 'linear-gradient(180deg,rgba(32,33,36,0) 29.18%,rgba(32,33,36,.9))',
  },
  titleWrap: {
    margin: theme.spacing(0, 1)
  },
  image: {
    width: '100%',
    height: '100%',
    objectFit: 'cover'
  },
}))

function CategoryHighlights(props) {
  const classes = useStyles(props);
  const { categories } = useMockState();
  const activeTab = useActiveTab(categories);
  return (
    <ScrollableGridList cols={3.5} cellHeight={80} spacing={8}>
      {(categories[activeTab].children || getRandomItems(categories.map(c => (c.children || [])).flat(), 8)).map((child) => (
        <GridListTile key={child.name} className={classes.gridListTile}>
          <Link to={`/explore/category`}>
            <img src={child.image} alt={child.label} className={classes.image} />
            <GridListTileBar
              title={child.label}
              classes={{
                root: classes.titleBar,
                title: classes.title,
                titleWrap: classes.titleWrap
              }}
            />
          </Link>
        </GridListTile>
      ))}
    </ScrollableGridList>
  )
}

export default CategoryHighlights;
