import { Chip } from "@material-ui/core";
import ScrollableGridList from "components/ScrollableGridList";
import { useMockState } from "MockContext";

function BrandsList() {
  const { brands } = useMockState()
  return (
    <ScrollableGridList multiLine spacing={8}>
      {brands.map((brand, index) => (
        <Chip
          key={index}
          variant="outlined"
          size="medium"
          label={brand}
        />
      ))}
    </ScrollableGridList>
  )
}

export default BrandsList;
