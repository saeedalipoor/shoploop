import { makeStyles } from '@material-ui/core/styles';
import { useMockState } from "MockContext";
const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: props => props.cols === 1 ? 'col' : 'row',
    flexWrap: 'wrap',
    // gap: props => props.cols === 1 ? 0 : theme.spacing(0.),
  },
  post: {
    width: props => props.cols > 1 ? `${1 / props.cols * 100}%` : '100%'
  }
}))

function PostList({ cols, postComponent: Component }) {
  const classes = useStyles({ cols })
  const { posts: [post] } = useMockState();
  const posts = Array.from({ length: 10 }).fill(post);
  if (!posts || !posts.length) return null;
  return (
    <div className={classes.root}>
      {posts.map((post, index) => <Component data={post} key={index} className={classes.post} />)}
    </div>
  )
}

export default PostList
