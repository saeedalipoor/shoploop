import Tabs from 'components/Tabs';
import Tab from 'components/Tab';
import Link from 'components/Link';
import { useActiveTab } from 'hooks/useActiveTab';
import { useMockState } from 'MockContext';


const categoryPath = (root, path) => `${root}/${path.replace('/', '')}`.replace(/\/$/, '');

export default function CategoriesTabs() {
  const { categories } = useMockState();
  const activeTab = useActiveTab(categories)
  return (
    <Tabs
      value={activeTab}
      variant={window.innerWidth < 600 ? "scrollable" : "standard"}
      centered={window.innerWidth >= 600}
      scrollButtons="auto"
    >
      {categories.map((category, index) => (
        <Tab
          key={category.path}
          component={Link}
          to={categoryPath('/explore', category.path)}
          label={category.label}
        />
      ))}
    </Tabs>
  )
}



