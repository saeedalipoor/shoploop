import { lazy } from 'react';

const Home = lazy(() => import('./pages/Home'));
const Profile = lazy(() => import('./pages/Profile'));


const routes = [
  {
    path: '/explore/b/:name?/:?sub',
    component: Home,
    exact: true
  },
  {
    path: '/explore/c/:id?',
    component: Profile,
    exact: true
  },
  {
    path: '/explore/:sub?/:name?',
    component: Home
  }
]

export default routes;