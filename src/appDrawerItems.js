import { Create, Help, Home, InfoOutlined, ListAlt, Settings, Storefront, Work } from "@material-ui/icons";

const appDrawerItems = [
    {
      "label": "Home", "icon": <Home />
    },
    {
      "label": "Stores", "icon": <Storefront />
    },
    {
      "label": "Your saved items", "icon": <ListAlt />
    },
    {
      "label": "Orders", "icon": <Work />
    },
    {
      "label": "My Contributions", "icon": <Create />
    },
    {
      "label": "Settings", "icon": <Settings />
    },
    {
      "label": "About Google Shopping", "icon": <InfoOutlined />
    },
    {
      "label": "Help", "icon": <Help />
    }
  ]
  export default appDrawerItems;