import { Suspense } from "react";
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import { MockProvider } from "MockContext";
import routes, { defaultRoute } from 'routes';
import helperClasses from "cssHelpers.js"
import { makeStyles } from "@material-ui/core/styles";
import mockData from "./mockData.json"
import { CircularProgress } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  '@global': helperClasses(theme)
}));

export default function App() {
  useStyles();
  return (
    <MockProvider initial={mockData}>
      <Router>
        <Suspense fallback={<div className="fixed inset-0 flex items-center justify-center"><CircularProgress /></div>}>
          <Switch>
            {process.env.REACT_APP_DEFAULT_ROUTE && <Redirect exact from="/" to={process.env.REACT_APP_DEFAULT_ROUTE} />}
            {routes.map((route) => <Route {...route} key={route.path} />)}
          </Switch>
        </Suspense>
      </Router>
    </MockProvider>
  );
}
