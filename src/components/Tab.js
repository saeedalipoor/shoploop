import { Tab } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles'
import { TheatersRounded } from "@material-ui/icons";

const StyledTab = withStyles((theme) => ({
  root: {
    minHeight: theme.spacing(5),
    minWidth: theme.spacing(2),
    textTransform: 'none',
    padding: theme.spacing(0, 1),    
    fontWeight: theme.typography.fontWeightMedium,
    // TODO: Should update margin for RTL layouts
    "&:first-child":{
      marginLeft: theme.spacing(2)
    },
    marginLeft: theme.spacing(1)

  },
}))((props) => <Tab {...props} />);

export default StyledTab;
