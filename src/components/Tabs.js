import { Tabs } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

const StyledTabs = withStyles(theme=>({
  root:{
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
    minHeight: theme.spacing(5)
  },
  indicator: {
    backgroundColor: 'transparent',
    display: 'flex',
    height: 4,
    paddingRight: theme.spacing(1),
    paddingLeft: theme.spacing(1),
    '& > span': {
      flexGrow: 1,
      backgroundColor: theme.palette.primary.main,
      borderRadius: '4px 4px 0 0',
    },
  },
}))(props => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

export default StyledTabs;