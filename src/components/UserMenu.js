import { ListItemIcon, ListItemText, Menu, MenuItem } from "@material-ui/core";
import { AccountCircleOutlined, HelpOutlineOutlined } from "@material-ui/icons";
import { useState } from "react";
import Avatar from "./Avatar";


export default function AvatarMenu() {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <>
      <Avatar
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleMenu}
        alt="Remy Sharp"
        src="/static/images/avatar/1.jpg"
        size={4}
      >
        SA
      </Avatar>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
          <ListItemIcon><AccountCircleOutlined /></ListItemIcon>
          <ListItemText primary="Creator Profile" />
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <ListItemIcon><HelpOutlineOutlined /></ListItemIcon>
          <ListItemText primary="Help" />
        </MenuItem>
      </Menu>
    </>
  )
}
