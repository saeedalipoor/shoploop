import { Button, Container, IconButton, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Launch as LaunchIcon, PlayCircleOutline, Share as ShareIcon, VolumeUp } from '@material-ui/icons';
import clsx from 'clsx';
import React, { Fragment } from 'react';
import Link from 'components/Link';
import Avatar from './Avatar';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  article: {
    maxWidth: '100%',
    position: 'relative',
    padding: theme.spacing(0, 0.5, 2)
  },
  metadata: {
    marginTop: theme.spacing(0.5),
    color: theme.palette.grey[600],
    "&>*": {
      whiteSpace: 'nowrap'
    }
  },
  metadataSpacer: {
    padding: theme.spacing(0, 0.5)
  },
  offerMeta: {
    display: 'flex',
    alignItems: 'center'
  },
  content: {
    overflow: 'hidden',
    flexGrow: 1,
    "& header": {
      overflow: 'hidden'
    }
  },
  media: {
    borderRadius: 12,
    overflow: 'hidden',
    position: 'relative',
    '& img': {
      display: 'block',
      width: '100%',
      height: '100%',
      objectFit: 'cover'
    }
  },
  mediaOverlay:{
    color: theme.palette.grey[50],
    padding: theme.spacing(1),
  },
  mediaOverlayImage: {
    flexShrink: '1',
    overflow: 'hidden',
    flexBasis: 40,
    '&>img': {
      width: '100%',
      height: 'auto',
      display: 'block',
      objectFit: 'cover',
      border: `4px solid ${theme.palette.grey[50]}`,
      borderRadius: 4,
    }
  },
  bigPlay:{
    filter: 'drop-shadow(0px 2px 1px rgba(0,0,0,0.3))',
    fontSize: theme.spacing(7)
  },
  bottomRow: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    gap: theme.spacing(1)
  },
  hashtags: {
    display: 'flex',
    alignItems: 'center',
    gap: theme.spacing(0, 1),
    padding: theme.spacing(1, 0, 2, 0),
    marginBottom: theme.spacing(-1),
    overflowX: 'auto'
  },
  hashtag: {
    textDecoration: 'none',
    color: theme.palette.grey[700],
    fontSize: theme.typography.body1.fontSize
  },
  avatar: {

  }
}))

function SmallPost({ data, className, ...props }) {
  const classes = useStyles()
  return (
    <article className={clsx(className, classes.article)} {...props}>
      
      <div className={classes.content}>
        <div className={classes.media}>
          <img src={data.images.large} />
          <div className={clsx(classes.mediaOverlay, 'inset-0 absolute flex flex-col justify-end')}>
            <div className="flex justify-between items-end">
              <div className={classes.mediaOverlayImage}>
                <img src={data.images.small} />
              </div>              
            </div>
          </div>
        </div>

        <header>
          <Typography component="h1" variant="h2">{data.title}</Typography>
          {data.metadata &&
            <Typography className={classes.metadata} variant="subtitle1" noWrap>
              {data.metadata.map((meta, index) => (
                <Fragment key={index}>
                  <span>{meta}</span>
                  {(index < data.metadata.length - 1) && <span className={classes.metadataSpacer}> • </span>}
                </Fragment>
              ))}
            </Typography>
          }
          <div className={clsx(classes.metadata, classes.offerMeta)}>
            <Typography variant="body1">{data.price}</Typography>
          </div>
        </header>
      </div>
      <Link to={`/explore/c/${data.author}`} className="absolute inset-0"/>    
    </article>
  )
}

export default SmallPost;
