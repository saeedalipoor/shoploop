import { forwardRef } from 'react';
import { Link as LinkBase } from 'react-router-dom'
import { useLocation } from "react-router-dom";

export default forwardRef(function Link(props, ref) {
  let location = useLocation();
  return (
    <LinkBase {...{
      ...props,
      innerRef:ref,
      to: {
        pathname: props.to.pathname || props.to,
        state: {
          from: location.pathname
        }
      }
    }} />
  )
}) 
