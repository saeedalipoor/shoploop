import { Chip, Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import mockData from "mockData.json";


const useStyles = makeStyles(theme => ({
  root: {
    overflowX: 'auto',
    overflowY: 'hidden'
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    gap: theme.spacing(1),
    width: '350vw',
    maxWidth: 1400,
    padding: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(2, 0),
    }
  }
}))
function BrandsList({ classes, className, ...props }){
  const { brands } = mockData;
  if (!brands || !brands.length) return null;
  return (
    <Container disableGutters maxWidth="sm" {...props} className={clsx(classes.root, className)}>
      <div className={classes.wrapper}>
        {brands.map((brand, index) => (
          <Chip
            key={index}
            variant="outlined"
            size="medium"
            label={brand}
          />
        ))}
      </div>
    </Container>
  )
}

export default BrandsList;
