import { Avatar as MuiAvatar } from "@material-ui/core"
import { withStyles } from '@material-ui/core/styles'
import clsx from "clsx";
import stringColor from "utils/stringColor";


const Avatar = withStyles((theme) => ({
  avatar: {
    width: props => theme.spacing(props.size || 4),
    height: props => theme.spacing(props.size || 4),
    cursor: 'pointer',
    backgroundColor: props => props.children?stringColor(props.children):theme.palette.primary.main
  }
}))(({ classes,className, size, children, ...props }) => (
  <MuiAvatar className={clsx(className, classes.avatar)} {...props}>
    {children && children[0].toUpperCase()}
  </MuiAvatar>
))

export default Avatar;