import { Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";


const useStyles = makeStyles(theme => ({
  root: {
    padding: props => props.title ? theme.spacing(2, 0, 0, 0) : 'inherit',
    backgroundColor: props => eval(`theme.palette.${props.bg}`),
    color: props => props.bg ? theme.palette.getContrastText(eval(`theme.palette.${props.bg}`)) : 'inherit'
  },
  title: {
    padding: props => props.disableGutters ? theme.spacing(0, 2) : 0
  }
}));
function PageSection({ title, children, disableGutters, bg, ...props }) {
  const classes = useStyles({ disableGutters, title, bg });
  return (
    <section className={classes.root} {...props}>
      <Container maxWidth="sm" disableGutters={disableGutters}>
        {title &&
          <Typography variant="h1" className={classes.title}>{title}</Typography>
        }
        {children}
      </Container>
    </section>
  )
}

export default PageSection;