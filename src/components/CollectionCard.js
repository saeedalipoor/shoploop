import { Button, Card, CardHeader } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles"
import { MoreHoriz } from "@material-ui/icons";
import clsx from "clsx";
import Link from "./Link";
import Avatar from "./Avatar";
import CardProgress from "./CardProgress";
import { useEffect, useState } from "react";


const useStyles = makeStyles(theme => ({
  root: {
    overflow: 'hidden',
    maxWidth: 287,
    display: 'flex',
    flexShrink: 0,
    position: 'relative',
    margin: props => `0 ${props.spacing / 2}px ${props.spacing}px ${props.spacing / 2}px`,
    [theme.breakpoints.up('sm')]: {
      maxWidth: 340,
    },
  },
  slides: {
    overflow: 'hidden',
    borderRadius: 12,
    "& img": {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    },
  },
  overlay: {
    zIndex: 1,
    display: 'flex',
    flexDirection: 'column-reverse',
    justifyContent: 'space-between',
    padding: theme.spacing(2)
  },
  overlayHeader: {
    padding: theme.spacing(1)
  },
  overlayTitle: {
    fontSize: theme.typography.h2.fontSize,
    fontWeight: theme.typography.fontWeightMedium
  },
  overlaySubtitle: {
    marginTop: theme.spacing(0.5),
    fontSize: theme.typography.body1.fontSize,
  },
  overlayAction: {
    alignSelf: 'auto'
  },
  overlayBottomRow: {
    display: 'flex',
    gap: props => theme.spacing(0.5),
    padding: theme.spacing(1),
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomOverlayImage: {
    flexShrink: '1',
    overflow: 'hidden',
    flexBasis: '16.666667%',
    '&>img': {
      width: '100%',
      height: 'auto',
      display: 'block',
      objectFit: 'cover',
      border: `1px solid ${theme.palette.grey[300]}`,
      borderRadius: 4,
      padding: 2
    }
  },
  storyProgress: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: 4,
    display: 'flex',
    gap: 3,
    margin: theme.spacing(1.5, 2),
    "& >*": {
      flexGrow: 1
    }
  }
}));
let progressInterval;
function CollectionCard({ data, spacing, ...props }) {
  const classes = useStyles();
  const initialProgress = Array.from({ length: data.images.large.length }).fill(0);
  const [cardProgress, setCardProgress] = useState(initialProgress)
  const [hideProgress, setHideProgress] = useState(false);

  useEffect(() => {
    // Just a temporary function for animating card progress bars
    const progressTimeout = setTimeout(() => {
      if (hideProgress) {
        if (cardProgress[cardProgress.length - 1] === 100) {
          setCardProgress(initialProgress);
        } else {
          setHideProgress(false)
        }
      } else {
        if (cardProgress[cardProgress.length - 1] === 100) {
          setTimeout(() => {
            setHideProgress(true);
          }, 1);
        } else {
          if (hideProgress) {
            setCardProgress(initialProgress)
          } else {
            setHideProgress(false);
            setCardProgress([
              ...cardProgress.slice(0, cardProgress.indexOf(0)),
              100,
              ...cardProgress.slice(cardProgress.indexOf(0) + 1)
            ])
          }
        }
      }
    }, 1000);
    return () => {
      clearTimeout(progressTimeout);
    }
  }, [])

  return (
    <div className={classes.root} {...props}>
      <div className={clsx(classes.slides, 'absolute inset-0')}>
        <div className={classes.storyProgress}>
          {data.images.large.map((_, i) => (
            <CardProgress key={i} variant="determinate" hideProgress={hideProgress} value={cardProgress[i]} />
          ))}
        </div>
        <img src={data.images.large[!cardProgress.includes(0) ? 0 : cardProgress.indexOf(0)]} />
      </div>
      <div className={classes.overlay}>
        <Card>
          <CardHeader
            classes={{
              avatar: classes.avatar,
              root: classes.overlayHeader,
              action: classes.overlayAction,
              title: classes.overlayTitle,
              subheader: classes.overlaySubtitle
            }}
            avatar={
              <Avatar aria-label={data.id}>
                {data.id}
              </Avatar>
            }
            action={<Button color="primary">Visit</Button>}
            title={data.id}
            subheader={data.categories}
          />
          <div className={classes.overlayBottomRow}>
            {data.images.small.map((src, index) => (
              <div key={index} className={classes.bottomOverlayImage}>
                <img src={src} />
              </div>
            ))}
            <MoreHoriz fontSize="small" color="action" />
          </div>
        </Card>
        <Link to={`/explore/c/${data.id}`} className="absolute inset-0 text-transparent">{data.id}</Link>
      </div>
    </div>
  )
}

export default CollectionCard;