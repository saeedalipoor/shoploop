import { GridList } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import useViewportState from "hooks/useViewportState";

const useStyles = makeStyles(theme => ({
  gridWrapper: {
    overflowX: 'auto',
    overflowY: 'hidden'
  },
  gridList: {
    flexWrap: 'nowrap',
    /* Workaround to prevent ignoring last child margin in flex grid */
    display: 'inline-flex',
    overflowX: 'visible',
    overflowY: 'hidden',
    width: 'max-content',
    gap: props => props.spacing,
    padding: props => props.xs ? theme.spacing(2) : theme.spacing(2, 0)
  },
  multiLineWrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    gap: props => props.spacing,
    width: '350vw',
    maxWidth: 1400,
    padding: props => props.xs ? `${theme.spacing(2)}px ${props.spacing * 2}px` : theme.spacing(2, 0),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(2, 0),
    }
  }
}))

function ScrollableGridList({ cols, cellHeight, spacing, children, multiLine, ...props }) {
  const { viewport: { isXs } } = useViewportState();
  const classes = useStyles({ cols, cellHeight, spacing, multiLine, xs: isXs });
  if (multiLine) return (
    <div className={classes.gridWrapper}>
      <div className={classes.multiLineWrapper}>
        {children}
      </div>
    </div>
  )
  return (
    <div className={classes.gridWrapper}>
      <GridList classes={{ root: classes.gridList }} {...{ cols, cellHeight, spacing: 0 }}>
        {children}
      </GridList>
    </div>
  )
}

export default ScrollableGridList;
