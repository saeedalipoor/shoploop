import { AppBar, Drawer, IconButton, List, ListItem, ListItemIcon, ListItemText, Toolbar, Typography } from "@material-ui/core";
import { useHistory, useLocation } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles'
import MenuIcon from '@material-ui/icons/Menu';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import { useState } from "react";
import UserMenu from "./UserMenu";
import { ArrowBack as ArrowBackIcon, Home as HomeIcon } from "@material-ui/icons";
import Link from "./Link";
import appDrawerItems from 'appDrawerItems'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  appBar: {
    backgroundColor: theme.palette.background.default,
    color: theme.palette.getContrastText(theme.palette.background.default),
    boxShadow: theme.shadows[0]
  },
  logo: {
    display: 'block',
    height: theme.spacing(3),
    width: 'auto'
  }
}));

export default function Appbar({ title, showBackButton, showAppLogo, children }) {
  let history = useHistory();
  let location = useLocation();
  const classes = useStyles();
  const [drawerState, setDrawerState] = useState(false);
  const toggleDrawer = (state) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setDrawerState(state || !drawerState)
  }
  const goBack = () => {
    history.goBack();
  }
  return (
    <>
      <AppBar className={classes.appBar}>
        <Toolbar>
          {!showBackButton &&
            <IconButton edge="start" color="inherit" aria-label="menu" className={classes.menuButton} onClick={toggleDrawer(true)}>
              <MenuIcon />
            </IconButton>
          }
          {/* Show back button if there is internal location in history */}
          {showBackButton && location.state && location.state.from &&
            <IconButton edge="start" color="inherit" aria-label="back" className={classes.menuButton} onClick={goBack}>
              <ArrowBackIcon />
            </IconButton>
          }
          {/* Show Home if the current location is very begining */}
          {showBackButton && !(location.state && location.state.from) &&
            <IconButton component={Link} to="/" edge="start" color="inherit" aria-label="home" className={classes.menuButton}>
              <HomeIcon />
            </IconButton>
          }
          <Typography variant="h6" className={classes.title}>
            {showAppLogo && <img src="/logo.svg" className={classes.logo} />}
            {title}
          </Typography>
          <div>
            <UserMenu />
          </div>
        </Toolbar>
        {children}
      </AppBar>
      {/* Empty toolbar to push content down when AppBar is fixed */}
      <Toolbar style={{ pointerEvents: 'none' }} />
      <Drawer anchor="left" open={drawerState} onClose={toggleDrawer(false)}>
        <div
          role="presentation"
          onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer(false)}
        >
          <List>
            <ListItem><img src="/logo.svg" /></ListItem>
            {appDrawerItems.map(({label, icon}, index) => (
              <ListItem button key={index}>
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText primary={label} />
              </ListItem>
            ))}
          </List>
        </div>
      </Drawer>
    </>
  )
}
