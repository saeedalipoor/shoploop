import { LinearProgress } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

const CardProgress = withStyles((theme) => ({
  root: {
    backgroundColor: 'rgba(255,255,255,0.5)',
  },
  bar: {
    backgroundColor: "#fff",
    transition: 'all 0.3s linear',
    opacity: props => props.hideProgress ? 0 : 1
  },
}))(({ hideProgress, ...props }) => <LinearProgress {...props} />);

export default CardProgress;