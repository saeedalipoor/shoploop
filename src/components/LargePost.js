import { Button, Container, IconButton, Typography } from '@material-ui/core';
import { Launch as LaunchIcon, PlayCircleOutline, Share as ShareIcon, VolumeUp } from '@material-ui/icons';
import clsx from 'clsx';
import React, { Fragment } from 'react';
import Link from 'components/Link';
import Avatar from './Avatar';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  article: {
    borderBottom: `1px solid ${theme.palette.grey[200]}`,
    width: '100%'
  },
  container: {
    display: 'flex',
    position: 'relative',
    gap: theme.spacing(2),
    padding: theme.spacing(2),
  },
  metadata: {
    marginTop: theme.spacing(0.5),
    color: theme.palette.grey[600],
    "&>*": {
      whiteSpace: 'nowrap'
    }
  },
  metadataSpacer: {
    padding: theme.spacing(0, 0.5)
  },
  offerMeta: {
    display: 'flex',
    alignItems: 'center'
  },
  content: {
    overflow: 'hidden',
    flexGrow: 1,
    "& header": {
      overflow: 'hidden'
    }
  },
  media: {
    borderRadius: 12,
    overflow: 'hidden',
    position: 'relative',
    '& img': {
      display: 'block',
      width: '100%',
      height: '100%',
      objectFit: 'cover'
    }
  },
  mediaOverlay: {
    color: theme.palette.grey[50],
    filter: 'drop-shadow()',
    padding: theme.spacing(2),
    "& div": {
      width: '100%'
    }
  },
  mediaOverlayImage: {
    flexShrink: '1',
    overflow: 'hidden',
    flexBasis: '16.666667%',
    '&>img': {
      width: '100%',
      height: 'auto',
      display: 'block',
      objectFit: 'cover',
      border: `4px solid ${theme.palette.grey[50]}`,
      borderRadius: 4,
    }
  },
  bigPlay: {
    filter: 'drop-shadow(0px 2px 1px rgba(0,0,0,0.3))',
    fontSize: theme.spacing(7)
  },
  bottomRow: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    gap: theme.spacing(1)
  },
  hashtags: {
    display: 'flex',
    alignItems: 'center',
    gap: theme.spacing(0, 1),
    padding: theme.spacing(1, 0, 2, 0),
    marginBottom: theme.spacing(-1),
    overflowX: 'auto'
  },
  hashtag: {
    textDecoration: 'none',
    color: theme.palette.grey[700],
    fontSize: theme.typography.body1.fontSize
  },
  avatar: {

  }
}))

function LargePost({ data, className, ...props }) {
  const classes = useStyles()
  return (
    <article className={clsx(className, classes.article)} {...props}>
      <Container maxWidth="sm" className={classes.container}>
        <Avatar aria-label={data.author} size={5}>
          {data.author}
        </Avatar>
        <div className={classes.content}>
          <header>
            <Typography component="h1" variant="h2">{data.title}</Typography>
            {data.metadata &&
              <Typography className={classes.metadata} variant="subtitle1" noWrap>
                {data.metadata.map((meta, index) => (
                  <Fragment key={index}>
                    <span>{meta}</span>
                    {(index < data.metadata.length - 1) && <span className={classes.metadataSpacer}> • </span>}
                  </Fragment>
                ))}
              </Typography>
            }
            <div className={clsx(classes.metadata, classes.offerMeta)}>
              <Typography variant="body1">{data.price}</Typography>
              <span className={classes.metadataSpacer}> • </span>
              <Button
                variant="text"
                color="primary"
                size="small"
                component="a"
                target="_blank"
                rel="noreferrer noopener"
                href={data.link.path}
                className={classes.button}
                endIcon={<LaunchIcon />}
              >
                {data.link.label}
              </Button>
            </div>
          </header>
          <div className={classes.media}>
            <img src={data.images.large} />
            <div className={clsx(classes.mediaOverlay, 'inset-0 absolute flex flex-col justify-between items-center')}>
              <div></div>
              <div className="text-center"><PlayCircleOutline className={classes.bigPlay} /></div>
              <div className="flex justify-between items-end">
                {data.images.small &&
                  <div className={classes.mediaOverlayImage}>
                    <img src={data.images.small} />
                  </div>
                }
                <Button
                  variant="outlined"
                  color="inherit"
                  size="small"
                  endIcon={<VolumeUp />}
                >
                  Watch
                </Button>
              </div>
            </div>
          </div>
          <div className={classes.bottomRow}>
            {data.hashtags &&
              <div className={classes.hashtags}>
                {data.hashtags.map(tag => (
                  <Link key={tag} to={`/explore/h/${tag}`} className={classes.hashtag}><span>#</span>{tag}</Link>
                ))}
              </div>
            }
            <IconButton size="small">
              <ShareIcon />
            </IconButton>
          </div>
        </div>
        <Link to={`/explore/c/${data.author}`} className="absolute inset-0" />
      </Container>
    </article>
  )
}

export default LargePost
