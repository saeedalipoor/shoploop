import React from 'react';

import Appbar from 'components/Appbar';
import { Divider, Toolbar } from '@material-ui/core';
import CategoriesTabs from 'containers/CategoriesTabs';
import PageSection from 'components/PageSection';
import CategoryHighlights from 'containers/CategoryHighlights';
import CollectionHighlights from 'containers/CollectionHighlights';
import BrandsList from 'containers/BrandsList';
import PostList from 'containers/PostList';
import LargePost from 'components/LargePost';
import useViewportState from 'hooks/useViewportState';

export default function Home(props) {
  const { viewport: { isXs } } = useViewportState();
  return (
    <>
    <Appbar showAppLogo>
    <CategoriesTabs />
    </Appbar>
    <Toolbar variant="dense" />
    
      <PageSection disableGutters={isXs}>
        <CategoryHighlights />
      </PageSection>

      <PageSection disableGutters={isXs} title="Top Collection" bg="grey[200]">
        <CollectionHighlights />
      </PageSection>

      <PageSection disableGutters={isXs} title="Popular brands">
        <BrandsList />
      </PageSection>

      <Divider style={{ height: 7 }} />

      <PageSection disableGutters>
        <PostList cols={isXs?1:2} postComponent={LargePost} />
      </PageSection>
      
    </>
  );
}