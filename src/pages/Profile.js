import { Button, Chip, Container, Divider, SvgIcon, Typography } from "@material-ui/core";
import Appbar from "components/Appbar";
import Avatar from "components/Avatar";
import PostsList from "containers/PostList";
import { useMockDispatch, useMockState } from "MockContext";
import { makeStyles } from "@material-ui/core/styles";
import { useState } from "react";
import SmallPost from "components/SmallPost";
import ScrollableGridList from "components/ScrollableGridList"
import PageSection from "components/PageSection";

const useStyles = makeStyles((theme) => ({
  header: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    gap: theme.spacing(1.5),
    marginBottom: theme.spacing(3)
  },
  verified: {
    fontSize: '0.85rem',
    width: 20
  },
  follow: {
    borderRadius: 30
  },
  bio: {
    position: 'relative',
    overflow: 'hidden',
    maxHeight: props => !props.showCompleteBio ? theme.spacing(4) : 600,
    cursor: props => !props.showCompleteBio ? 'cursor' : 'default',
    transition: 'max-height 0.5s linear'
  },
  showMore: {
    position: 'absolute',
    right: 0,
    top: theme.spacing(2),
    background: 'linear-gradient(-90deg,#fff 80%,rgba(255,255,255,0))',
    padding: '0px 16px 0 28px',
  }
}))



function Profile(props) {
  const [showCompleteBio, setShowCompleteBio] = useState(false)
  const classes = useStyles({ showCompleteBio });
  const { users } = useMockState()
  const dispatch = useMockDispatch()
  const { match: { params: { id } } } = props;
  const user = users.filter(usr => usr.id === id)[0];
  return (
    <>
      <Appbar showBackButton />
      <Container maxWidth="sm">
        <header className={classes.header}>
          <Avatar size={9}>{user.id}</Avatar>
          <div className="flex items-center">
            <Typography>{user.id}</Typography>
            <SvgIcon className={classes.verified} color="primary">
              <path d="M23 12L20.56 9.21L20.9 5.52L17.29 4.7L15.4 1.5L12 2.96L8.6 1.5L6.71 4.69L3.1 5.5L3.44 9.2L1 12L3.44 14.79L3.1 18.49L6.71 19.31L8.6 22.5L12 21.03L15.4 22.49L17.29 19.3L20.9 18.48L20.56 14.79L23 12ZM10.09 16.72L6.29 12.91L7.77 11.43L10.09 13.76L15.94 7.89L17.42 9.37L10.09 16.72Z" />
            </SvgIcon>
          </div>
          <Button className={classes.follow} color="primary" variant={user.following?"outlined":"contained"} disableElevation onClick={() => { dispatch({ type: 'TOGGLE_FOLLOW', payload: user.id})}}>
            {user.following ? 'Following' : 'Follow'}
          </Button>
          <div className={classes.bio} onClick={() => setShowCompleteBio(true)}>
            <Typography variant="subtitle1" align="center" color="textSecondary">{user.bio}</Typography>
            {!showCompleteBio && <Typography variant="body1" align="center" className={classes.showMore}>Show more</Typography>}
          </div>
        </header>
      </Container>


      <Divider style={{ height: 7 }} />

      <PageSection disableGutters>
        <ScrollableGridList cols={4.5} spacing={8} cellHeight={26}>
          {user.categories.map((cat, index) => (
            <Chip
              key={index}
              variant="outlined"
              size="medium"
              label={cat}
            />
          ))}
        </ScrollableGridList>
      </PageSection>

      <PageSection>
        <PostsList cols={2} postComponent={SmallPost} />
      </PageSection>
    </>
  )
}

export default Profile;