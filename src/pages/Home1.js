import { useState, useEffect } from "react";
import Link from "components/Link";
import Tab from 'components/Tab'
import Tabs from 'components/Tabs'
import mockData from "mockData.json";
import { Chip, Container, Divider, GridList, GridListTile, GridListTileBar, Toolbar, Typography } from "@material-ui/core";
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Appbar from "components/Appbar";
import CollectionCard from "components/CollectionCard";
import { AvTimerTwoTone } from "@material-ui/icons";
import BrandsList from "components/BrandsList";

function getRandomItems(array, count = 1) {
  let remainingArray = [...array];
  let res = [];
  for (let i = 0; i < count; i++) {
    const randomIndex = Math.floor(Math.random() * remainingArray.length);
    res.push(remainingArray.splice(randomIndex, 1)[0])
  }
  return res;
}

const categoryPath = (root, path) => `${root}/${path.replace('/', '')}`.replace(/\/$/, '');
const getAtiveTabIndexByUrl = (match, categories) => (match.params.name ? categories.findIndex(category => match.params.name === category.name) : 0);
const useStyles = makeStyles((theme) => ({
  gridWrapper: {
    overflowX: 'auto',
    overflowY: 'hidden'
  },
  gridList: {
    flexWrap: 'nowrap',
    /* Workarount to prevent ignoring last child margin in flex grid */
    display: 'inline-flex',
    overflowX: 'visible',
    overflowY: 'hidden',
    width: 'max-content',
    padding: window.innerWidth < 600 ? theme.spacing(2.3, 2) : theme.spacing(2.3, 0)
  },
  gridListTile: {
    maxWidth: 108,
    minWidth: 108,
    [theme.breakpoints.up('sm')]: {
      maxWidth: 130,
      minWidth: 130,
    },
  },
  title: {
    color: theme.palette.grey[50],
    fontSize: theme.typography.body1.fontSize,
    fontWeight: theme.typography.fontWeightMedium,
    paddingBottom: theme.spacing(1),
    whiteSpace: 'pre-wrap',
    lineHeight: theme.typography.body1.lineHeight
  },
  titleBar: {
    textAlign: 'center',
    alignItems: 'flex-end',
    height: '100%',
    background: 'linear-gradient(180deg,rgba(32,33,36,0) 29.18%,rgba(32,33,36,.9))',
  },
  titleWrap: {
    margin: theme.spacing(0, 1)
  },
  image: {
    width: '100%',
    height: '100%',
    objectFit: 'cover'
  },
  logo: {
    display: 'block',
    height: theme.spacing(3),
    width: 'auto'
  }
}));


const PageSection = withStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 0, 0),
    backgroundColor: props => eval(`theme.palette.${props.bg}`)
  }
}))(({ classes, bg, title, children, ...props }) => (
  <section className={classes.root} {...props}>
    <Container maxWidth="sm">
      <Typography variant="h1">{title}</Typography>
    </Container>
    {children}
  </section>
))


export default function Home(props) {
  const classes = useStyles();

  const { categories, collections } = mockData;
  const [activeTab, setActiveTab] = useState(getAtiveTabIndexByUrl(props.match, categories))
  useEffect(() => {
    setActiveTab(getAtiveTabIndexByUrl(props.match, categories));
    return () => { }
  }, [props.match])
  return (
    <>
      <Appbar title={<img src="/logo.svg" className={classes.logo} />}>
        <Tabs
          value={activeTab}
          variant={window.innerWidth < 600 ? "scrollable" : "standard"}
          centered={window.innerWidth >= 600}
          scrollButtons="auto"
        >
          {categories.map((category, index) => (
            <Tab
              key={category.path}
              component={Link}
              to={categoryPath('/explore', category.path)}
              label={category.label}
            />
          ))}
        </Tabs>
      </Appbar>
      <Toolbar variant="dense" />

      <Container maxWidth="sm" disableGutters={window.innerWidth < 600}>
        <div className={classes.gridWrapper}>
          <GridList className={classes.gridList} cols={3.5} cellHeight={80} spacing={8}>
            {(categories[activeTab].children || getRandomItems(categories.map(c => (c.children || [])).flat(), 8)).map((child) => (
              <GridListTile key={child.name} className={classes.gridListTile}>
                <Link to={`/explore/category`}>
                  <img src={child.image} alt={child.label} className={classes.image} />
                  <GridListTileBar
                    title={child.label}
                    classes={{
                      root: classes.titleBar,
                      title: classes.title,
                      titleWrap: classes.titleWrap
                    }}
                  />
                </Link>
              </GridListTile>
            ))}
          </GridList>
        </div>
      </Container>

      <PageSection bg="grey[200]" title="Top Collections">
        <Container maxWidth="sm" disableGutters={window.innerWidth < 600}>
          <div className={classes.gridWrapper}>
            <GridList className={classes.gridList} cols={1.3} cellHeight={353} spacing={8}>
              {collections.map(data => (
                <CollectionCard data={data} key={data.id} spacing={8} />
              ))}
            </GridList>
          </div>
        </Container>
      </PageSection>

      <PageSection title="Popular brands">
        <Container disableGutters>
          <BrandsList />
        </Container>
      </PageSection>

      <Divider style={{height: 7}}/>
      
    </>
  )
}
