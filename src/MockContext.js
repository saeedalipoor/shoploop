import { createContext, useContext, useReducer } from "react";

const MockContext = createContext();
const MockDispatchContext = createContext();


function dataReducer(state, action) {
  switch (action.type) {
    case 'TOGGLE_FOLLOW':
      let user = state.users.filter(u => u.id === action.payload)[0];
      user = { ...user, following: !user.following };
      const newState = {
        ...state,
        users: [
          ...state.users.filter(u => u.id !== action.payload),
          user
        ]
      }
      return { ...newState };
  }
}

function MockProvider({ children, initial = {} }) {
  const [state, dispatch] = useReducer(dataReducer, {
    ...initial
  });
  return (
    <MockContext.Provider value={state}>
      <MockDispatchContext.Provider value={dispatch}>
        {children}
      </MockDispatchContext.Provider>
    </MockContext.Provider>
  );
}

function useMockState() {
  const context = useContext(MockContext);
  if (context === undefined) {
    throw new Error("useCountState must be used within a CountProvider");
  }
  return context;
}

function useMockDispatch() {
  const context = useContext(MockDispatchContext);
  if (context === undefined) {
    throw new Error("useCountDispatch must be used within a CountProvider");
  }
  return context;
}

export { MockProvider, useMockState, useMockDispatch };
