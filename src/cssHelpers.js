export default function (theme) {
  return {
    '.inset-0': {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0
    },
    '.absolute': {
      position: 'absolute'
    },
    '.fixed': {
      position: 'fixed'
    },
    '.text-transparent': {
      color: 'transparent'
    },
    '.flex': {
      display: 'flex'
    },
    '.flex-row':{
      flexDirection: 'row'
    },
    '.flex-col':{
      flexDirection: 'column'
    },
    '.flex-row-reverse':{
      flexDirection: 'row-reverse'
    },
    '.flex-col-reverse':{
      flexDirection: 'column-reverse'
    },
    '.justify-between':{
      justifyContent: 'space-between'
    },
    '.justify-center':{
      justifyContent: 'center'
    },
    '.justify-end':{
      justifyContent: 'flex-end'
    },
    '.items-center':{
      alignItems: 'center'
    },
    '.items-end':{
      alignItems: 'flex-end'
    },
    '.text-center':{
      textAlign: 'center'
    }
  }
}