import { useTheme } from "@material-ui/core/styles";
import { useCallback, useEffect, useState } from "react";

export default function useViewportState() {
  /* TODO: Provide this hook as context */
  const { breakpoints: { values } } = useTheme();
  const resizeObserver = useCallback(
    () => {
      setState({
        isXs: window.innerWidth <= values.sm,
        isSm: window.innerWidth >= values.sm && window.innerWidth <= values.md,
        isMd: window.innerWidth >= values.md && window.innerWidth <= values.lg,
        isLg: window.innerWidth >= values.md && window.innerWidth <= values.xl,
        isxl: window.innerWidth >= values.xl
      })
    },
    [values],
  )
  const [state, setState] = useState(null)
  useEffect(() => {
    if(!state) resizeObserver()
    window.addEventListener('resize', resizeObserver, true)
    return () => {
      window.removeEventListener('resize', resizeObserver, true)
    }
  }, [])
  return { viewport: state||{} }
}