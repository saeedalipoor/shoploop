import { useEffect, useState } from "react";
import { useParams } from "react-router";

const getAtiveTabIndexByUrl = (params, categories) => (params.name ? categories.findIndex(category => params.name === category.name) : 0);
export function useActiveTab(categories) {
  const params = useParams();
  const [activeTab, setActiveTab] = useState(getAtiveTabIndexByUrl(params, categories))
  useEffect(() => {
    setActiveTab(getAtiveTabIndexByUrl(params, categories));
    return () => { }
  }, [params])
  return activeTab
}