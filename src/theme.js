import { createMuiTheme } from '@material-ui/core/styles';


const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#91c1ff',
      main: '#1a73e8',
      dark: '#2c65c2',
      contrastText: '#fff',
    },
    background: {
      default: '#fff',
    }
  },
  typography: {
    fontFamily: [
      'Google Sans',
      'Roboto',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    h1: {
      fontSize: `${18 / 16}rem`,
      fontWeight: 500,
      lineHeight: `${24 / 16}rem`
    },
    h2: {
      fontSize: `${16 / 16}rem`,
      fontWeight: 400,
      lineHeight: `${20 / 16}rem`
    },
    body1: {
      fontSize: `${14 / 16}rem`,
      fontWeight: 500,
      lineHeight: `${18 / 16}rem`
    },
    body2: {
      fontSize: `${12 / 16}rem`,
      fontWeight: 400,
      lineHeight: `${18 / 16}rem`
    },
    subtitle1: {
      fontSize: `${14 / 16}rem`,
      fontWeight: 400,
      lineHeight: `${16 / 16}rem`
    },
    subtitle2: {
      fontSize: `${12 / 16}rem`,
      fontWeight: 400,
      lineHeight: `${14 / 16}rem`
    },
  },
  props:{
    MuiButtonBase: {      
      disableRipple: true,
    }
  },
  overrides: {    
    MuiToolbar:{
      dense:{
        minHeight: 40
      }
    },
    MuiGridListTile: {
      tile: {
        borderRadius: '8px',
      }
    },
    MuiChip:{
      outlined:{
        border: '1px solid #bdc1c6',
        fontSize: `${14 / 16}rem`,
        fontWeight: 500
      }
    }
  }
});

export default theme;
