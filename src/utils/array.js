export const shuffleArray = arr => arr.sort(() => Math.random() - 0.5);

export default function getRandomItems(array, count = 1) {
  let shuffledArray = shuffleArray(array);
  return shuffledArray.splice(-1*count)
}
